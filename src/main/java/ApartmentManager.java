import model.Apartment;
import model.ApartmentType;
import model.GeoCoordinates;
import model.Infrastructure;
import model.requirement.DistanceRequirement;
import model.requirement.RentPriceRequirement;
import model.requirement.Requirement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ApartmentManager {
  private List<Apartment> apartments;
  private List<Infrastructure> infrastructures;

  public void populateData() {
    infrastructures = new ArrayList<>();
    apartments = new ArrayList<>();
    GeoCoordinates geoCoordinates = new GeoCoordinates(1, 1);
    Infrastructure infrastructure = new Infrastructure("School", geoCoordinates);
    infrastructures.add(infrastructure);

    BigDecimal moneyForRentPrice = new BigDecimal("100");
    BigDecimal moneyForSalePrice = new BigDecimal("2000");
    GeoCoordinates apartGeoCoordinatesXY = new GeoCoordinates(0, 0);
    Apartment apartment =
        new Apartment(
            "House",
            moneyForRentPrice,
            moneyForSalePrice,
            true,
            true,
            3,
            ApartmentType.HOUSE,
            apartGeoCoordinatesXY);
    apartments.add(apartment);
    for (int i = 1; i < 5; i++) {
      infrastructures.add(
          new Infrastructure(("Lorem ipsum" + i), new GeoCoordinates(i * 10, i * 15)));
      apartments.add(
          new Apartment(
              "Lorem ipsum" + i,
              new BigDecimal(String.valueOf(i * 100)),
              new BigDecimal(String.valueOf(i * 1000)),
              true,
              true,
              i,
              ApartmentType.FLAT,
              new GeoCoordinates(i * 15, i * 20)));
    }

    apartments.add(
        new Apartment.Builder()
            .withDescription("Test01")
            .withGeoXY(new GeoCoordinates(22, 33))
            .withSalePrice(new BigDecimal(5000))
            .withType(ApartmentType.PENTHOUSE)
            .withRentPrice(new BigDecimal(300))
            .build());
    apartments.add(Apartment.newBuilder().withDefaultParameters().build());
  }

  public List<Apartment> sortApartmentsRentPrice(List<Apartment> apartmentsForSort) {
    return apartmentsForSort.stream()
        .sorted((o1, o2) -> o1.getRentPrice().compareTo(o2.getRentPrice()))
        .collect(Collectors.toList());
  }

  public List<Apartment> sortApartmentsSalePrice(List<Apartment> apartmentsForSort) {
    return apartmentsForSort.stream()
        .sorted((o1, o2) -> o1.getSalePrice().compareTo(o2.getSalePrice()))
        .collect(Collectors.toList());
  }

  public List<Apartment> sortApartmentsSaleAndRentPrice(List<Apartment> apartmentsForSort) {
    return apartmentsForSort.stream()
        .sorted(
            Comparator.comparing(Apartment::getSalePrice).thenComparing(Apartment::getRentPrice))
        .collect(Collectors.toList());
  }

  public List<Apartment> doRealtor(List<Requirement> requirements) {
    return apartments.stream()
        .filter(
            apartment ->
                requirements.stream().allMatch(requirement -> requirement.matches(apartment)))
        .collect(Collectors.toList());
  }

  public void showTestData() {
    List<Requirement> requirements = new ArrayList<>();
    RentPriceRequirement rentPriceRequirement = new RentPriceRequirement(new BigDecimal(200d));
    requirements.add(rentPriceRequirement);

    Infrastructure school =
        infrastructures.stream()
            .filter(i -> i.getDescription().equals("School"))
            .findFirst()
            .orElse(null);

    DistanceRequirement distanceRequirement = new DistanceRequirement(45d, school);
    requirements.add(distanceRequirement);
    System.out.println("All apartments");
    System.out.println(apartments);
    System.out.println("All infrastructures");
    System.out.println(infrastructures);
    System.out.println("All requirements");
    System.out.println(requirements);

    System.out.println("All apartments that meet the requirements");
    System.out.println(doRealtor(requirements));

    System.out.println("Apartments sort by Rent Price");
    System.out.println(sortApartmentsRentPrice(apartments));

    System.out.println("Apartments sort by Sale Price");
    System.out.println(sortApartmentsSalePrice(apartments));

    System.out.println("Apartments sort by Sale Price and then by Rent Price");
    System.out.println(sortApartmentsSaleAndRentPrice(apartments));
  }
}
