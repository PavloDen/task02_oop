package service;

import model.Apartment;
import model.Infrastructure;

public class DistanceService {
  private static DistanceService distanceService;

  public DistanceService() {}

  public static DistanceService getDistanceService() {
    if (distanceService == null) {
      distanceService = new DistanceService();
    }
    return distanceService;
  }

  public double calculate(Apartment apartment, Infrastructure infrastructure) {

    double deltaX =
        apartment.getGeoCoordinates().getCoordinateX()
            - infrastructure.getGeoCoordinates().getCoordinateX();
    double deltaY =
        apartment.getGeoCoordinates().getCoordinateY()
            - infrastructure.getGeoCoordinates().getCoordinateY();
    return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
  }
}
