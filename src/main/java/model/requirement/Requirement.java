package model.requirement;

import model.Apartment;

public interface Requirement<T> {

  boolean matches(Apartment apartment);
}
