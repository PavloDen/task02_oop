package model.requirement;

import java.math.BigDecimal;
import model.Apartment;

public class SalePriceRequirement implements Requirement<BigDecimal> {
  private BigDecimal value;

  public SalePriceRequirement() {}

  public SalePriceRequirement(BigDecimal value) {
    this.value = value;
  }

  public boolean matches(Apartment apartment) {
    return apartment.getSalePrice().compareTo(value) <= 0;
  }

  public BigDecimal getValue() {
    return value;
  }

  public void setValue(BigDecimal value) {
    this.value = value;
  }
}
