package model.requirement;

import model.Apartment;
import model.Infrastructure;
import service.DistanceService;

public class DistanceRequirement implements Requirement<Double> {
  private Double distance;
  private Infrastructure infrastructure;

  public DistanceRequirement() {}

  public DistanceRequirement(Double distance, Infrastructure infrastructure) {
    this.distance = distance;
    this.infrastructure = infrastructure;
  }

  public boolean matches(Apartment apartment) {
    return DistanceService.getDistanceService().calculate(apartment, infrastructure) <= distance;
  }

  public Double getDistance() {
    return distance;
  }

  public void setDistance(Double distance) {
    this.distance = distance;
  }

  public Infrastructure getInfrastructure() {
    return infrastructure;
  }

  public void setInfrastructure(Infrastructure infrastructure) {
    this.infrastructure = infrastructure;
  }

  @Override
  public String toString() {
    return "model.requirement.DistanceRequirement{"
        + "distance="
        + distance
        + ", infrastructure="
        + infrastructure
        + '}';
  }
}
