package model.requirement;

import model.Apartment;

public interface Preference<T> {
  boolean matches(Apartment apartment);
}
