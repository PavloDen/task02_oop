package model.requirement;

import java.math.BigDecimal;
import model.Apartment;

public class RentPriceRequirement implements Requirement<BigDecimal> {

  private BigDecimal value;

  public RentPriceRequirement() {}

  public RentPriceRequirement(BigDecimal value) {
    this.value = value;
  }

  public boolean matches(Apartment apartment) {
    return apartment.getRentPrice().compareTo(value) <= 0;
  }

  public BigDecimal getValue() {
    return value;
  }

  public void setValue(BigDecimal value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "RentPriceRequirement{" + "value=" + value + '}';
  }
}
