package model;

import java.math.BigDecimal;

public class Apartment {
  private String description;
  private BigDecimal rentPrice;
  private BigDecimal salePrice;
  private boolean isForRent;
  private boolean isForSale;
  private int numberOfBedrooms;
  private ApartmentType type;
  private GeoCoordinates geoCoordinates;

  public Apartment() {}

  public Apartment(
      String description,
      BigDecimal rentPrice,
      BigDecimal salePrice,
      boolean isForRent,
      boolean isForSale,
      int numberOfBedrooms,
      ApartmentType type,
      GeoCoordinates geoCoordinates) {
    this.description = description;
    this.rentPrice = rentPrice;
    this.salePrice = salePrice;
    this.isForRent = isForRent;
    this.isForSale = isForSale;

    this.numberOfBedrooms = numberOfBedrooms;
    this.type = type;
    this.geoCoordinates = geoCoordinates;
  }

  public GeoCoordinates getGeoCoordinates() {
    return geoCoordinates;
  }

  public void setGeoCoordinates(GeoCoordinates geoCoordinates) {
    this.geoCoordinates = geoCoordinates;
  }

  public ApartmentType getType() {
    return type;
  }

  public void setType(ApartmentType type) {
    this.type = type;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getRentPrice() {
    return rentPrice;
  }

  public void setRentPrice(BigDecimal rentPrice) {
    this.rentPrice = rentPrice;
  }

  public BigDecimal getSalePrice() {
    return salePrice;
  }

  public void setSalePrice(BigDecimal salePrice) {
    this.salePrice = salePrice;
  }

  public boolean isForRent() {
    return isForRent;
  }

  public void setForRent(boolean forRent) {
    isForRent = forRent;
  }

  public boolean isForSale() {
    return isForSale;
  }

  public void setForSale(boolean forSale) {
    isForSale = forSale;
  }

  public int getNumberOfBedrooms() {
    return numberOfBedrooms;
  }

  public void setNumberOfBedrooms(int numberOfBedrooms) {
    this.numberOfBedrooms = numberOfBedrooms;
  }

  public static Builder newBuilder() {
    return new Builder();
  }

  public static class Builder {
    private String description;
    private BigDecimal rentPrice;
    private BigDecimal salePrice;
    private boolean isForRent;
    private boolean isForSale;
    private int numberOfBedrooms;
    private ApartmentType type;
    private GeoCoordinates geoCoordinates;

    public Builder() {}

    public Builder withDescription(String name) {
      this.description = name;
      return this;
    }

    public Builder withRentPrice(BigDecimal price) {
      this.rentPrice = price;
      this.isForRent = true;
      return this;
    }

    public Builder withSalePrice(BigDecimal price) {
      this.salePrice = price;
      this.isForSale = true;
      return this;
    }

    public Builder withGeoXY(GeoCoordinates geoXY) {
      this.geoCoordinates = geoXY;
      return this;
    }

    public Builder withType(ApartmentType type) {
      this.type = type;
      return this;
    }

    public Builder withBedrooms(int numberOfBedrooms) {
      this.numberOfBedrooms = numberOfBedrooms;
      return this;
    }

    public Builder withDefaultParameters() {
      this.description = "Default Test Name";
      this.rentPrice = new BigDecimal(555);
      this.salePrice = new BigDecimal(5555);
      this.isForSale = true;
      this.isForRent = true;
      this.numberOfBedrooms = 5;
      this.type = ApartmentType.FLAT;
      this.geoCoordinates = new GeoCoordinates(11, 22);

      return this;
    }

    public Apartment build() {
      return new Apartment(
          description,
          rentPrice,
          salePrice,
          isForRent,
          isForSale,
          numberOfBedrooms,
          type,
          geoCoordinates);
    }
  }

  @Override
  public String toString() {
    return "Apartment{"
        + "description='"
        + description
        + '\''
        + ", rentPrice="
        + rentPrice
        + ", salePrice="
        + salePrice
        + ", isForRent="
        + isForRent
        + ", isForSale="
        + isForSale
        + ", numberOfBedrooms="
        + numberOfBedrooms
        + ", type="
        + type
        + ", geoCoordinates="
        + geoCoordinates
        + '}'
        + "\n";
  }
}
