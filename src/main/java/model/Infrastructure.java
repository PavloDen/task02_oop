package model;

public class Infrastructure {
  private String description;
  private GeoCoordinates geoCoordinates;

  public Infrastructure() {}

  public Infrastructure(String description) {
    this.description = description;
  }

  public Infrastructure(String description, GeoCoordinates geoCoordinates) {
    this.description = description;
    this.geoCoordinates = geoCoordinates;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public GeoCoordinates getGeoCoordinates() {
    return geoCoordinates;
  }

  public void setGeoCoordinates(GeoCoordinates geoCoordinates) {
    this.geoCoordinates = geoCoordinates;
  }

  @Override
  public String toString() {
    return "Infrastructure{"
        + "description='"
        + description
        + '\''
        + ", geoCoordinates="
        + geoCoordinates
        + '}'
        + "\n";
  }
}
