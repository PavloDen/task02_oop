package model;

public enum ApartmentType {
    HOUSE,
    PENTHOUSE,
    FLAT
}
